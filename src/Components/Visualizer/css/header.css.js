export const nav__body={
    width:"100vw",height:"60px",display:'flex'
}

export const nav__title={
    backgroundColor:'#4CAF50',
    color:"#fff",
    display:'flex',justifyContent:'center',alignItems:'center',
    marginLeft:"10px",
    padding:'10px',
    borderBottomRightRadius:"5px",borderBottomLeftRadius:"5px",
    cursor:'pointer'
}

export const nav__icons={
    backgroundColor:"#15314b",
    color:"#fff",
    display:'flex',justifyContent:'center',alignItems:'center',
    marginLeft:"10px",
    padding:'20px',
    borderBottomRightRadius:"5px",borderBottomLeftRadius:"5px",
    cursor:'pointer'
}

export const link={
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    textDecoration: 'none',
    color:'#fff',
    width:'100%',
    height:'100%'
}