export const horizontalSeparator={
    width:'100%',height:'0.5vw'
}

export const verticalSeparator={
    width:'0.5vw',height:'100%'
}

export const codeLayout={
    display:'flex',justifyContent:'flex-start',alignItems:'flex-start',
    margin:10,
    height:'100%',
}

export const codeLayout__section_editor={
    display:'flex',justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'column',
    backgroundColor:'#001528',
    width:'60vw',
    height:'100%'
}

export const codeLayout__section_console={
    display:'flex',justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'column',
    backgroundColor:'#001528',
    width:'40vw',
    height:'315px'
}

export const codeLayout__section_datastructure={
    display:'flex',justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'column',
    backgroundColor:'#001528',
    width:'40vw',
    height:'315px'
}

export const nav={
    width:'100%',height:'50px',
    backgroundColor:'#15314b',
    display:'flex',justifyContent:'flex-end',alignItems:'center',
    boxShadow:'0 8px 6px -6px #030d16',zIndex:1000
}

export const nav_section1={
    display:'flex',justifyContent:'flex-start',height:'100%',alignItems:'center',flexGrow:1
}

export const nav_section2={
    display:'flex',justifyContent:'center',height:'100%',alignItems:'center'
}

export const nav__icon={
    backgroundColor:"#15314b",
    color:"#fff",
    display:'flex',justifyContent:'center',alignItems:'center',
    padding:'10px',cursor:'pointer'
}

export const nav__button={
    backgroundColor:"#0046c7",
    color:"#fff",
    display:'flex',justifyContent:'center',alignItems:'center',
    width:"120px",height:'100%',cursor:'pointer'
}

export const nav__text={
    display:'flex',justifyContent:'center',alignItems:'center',
    backgroundColor:'#001528',
    color:'#fff',
    height:'100%',
    width:'130px'
}